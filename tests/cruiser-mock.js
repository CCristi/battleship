class Ship {
  constructor(hitPoints) {
    this.hitPoints = new Set(hitPoints);
  }

  isSunk() {
    return this.hitPoints.size === 0;
  }

  hit(point) {
    if (this.hitPoints.has(point)) {
      this.hitPoints.delete(point);

      return this.isSunk() ? 'SUNK' : 'HIT';
    }

    return 'MISS';
  }
}

export default class CruiserMock {
  constructor(shipsHitPoints) {
    this.ships = [];

    for (const hitPoints of shipsHitPoints) {
      this.addShip(hitPoints);
    }
  }

  addShip(hitPoints) {
    this.ships.push(new Ship(hitPoints));
  }

  isSunk() {
    return this.ships.every(ship => ship.isSunk());
  }

  hit(point) {
    for (const ship of this.ships) {
      const res = ship.hit(point);

      if (['HIT', 'SUNK'].includes(res)) {
        return res;
      }
    }

    return 'MISS';
  }
}