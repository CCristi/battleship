import { expect } from 'chai';
import Player from '../players/Player1';
import CruiserMock from './cruiser-mock';

describe('Testing players required functions', () => {
  it('player should have method getName', () => {
    expect(Player.getName(), 'Invalid name').to.be.a('string');
  });

  it('should kill a 1x4 horizontal ship in a maximum of 1000 moves', () => {
    const player = new Player();
    const cruiser = new CruiserMock([
      ['E3', 'E4', 'E5', 'E6'],
    ]);

    for (let i = 0; i < 1000; i++) {
      const point = player.attack();

      expect(point, 'Invalid attack point').to.match(/^[A-K]([1-9]|10)$/);

      player.attackResult(cruiser.hit(point));

      if (cruiser.isSunk()) {
        break;
      }
    }

    expect(cruiser.isSunk(), 'Cruiser is still alive').to.be.true;
  });



  it('should kill a 1x4 vertical ship in a maximum of 1000 moves', () => {
    const player = new Player();
    const cruiser = new CruiserMock([
      ['E3', 'F3', 'G3', 'H3'],
    ]);

    for (let i = 0; i < 1000; i++) {
      const point = player.attack();

      expect(point, 'Invalid attack point').to.match(/^[A-K]([1-9]|10)$/);

      const result = cruiser.hit(point);

      player.attackResult(result);

      if (cruiser.isSunk()) {
        break;
      }
    }

    expect(cruiser.isSunk(), 'Cruiser is still alive').to.be.true;
  });
});
