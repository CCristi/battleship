import Player1 from './Player1';

class Igor extends Player1 {
  /**
   *
   * @param props
   */
  constructor(props) {
    super(props);
    this.attaks = [];
    this.generateStash();
  }

  /**
   *
   */
  generateStash() {
    const matrix = [...new Array(10)].map(() => [...new Array(10)].fill(0));

    this.stash = matrix.reduce((acc, row, rowIndex) => {
      row.forEach((_, columnIndex) => {
        const i = String.fromCharCode(65 + rowIndex);
        const j = columnIndex + 1;
        acc.push(`${ i }${ j }`);
      });
      return acc;
    }, []);
  }

  /**
   * MISS -> missed attack
   * HIT ->  hited ship but ship is not sunk yet
   * SUNK -> ship is dead
   * DUPLICATED -> once again you attacked this coordinates
   * @param {String} result one of HIT, MISS, SUNK, DUPLICATED.
   * @return {void}
   */
  attackResult(result) {
    this.registerAttackResult(result);
  }

  /**
   * Returns coordinates in which player want attack
   * First char is Letter=(A...J) rest chars represents a number=(0...10)
   * @return {string} for example: 'B10'
   */
  attack() {
    const index = this.getPointIndex();
    const point = this.stash[index];

    this.registerPointAsUsed(index);

    return point;
  }

  /**
   *
   * @return {number|*}
   */
  getPointIndex() {
    const { lastAttack } = this;
    if (lastAttack.result === 'HIT') {
      const { index } = lastAttack;
      if (this.stash[index]) {
        return index;
      }
    }
    const randomIndex = Math.floor(Math.random() * this.stash.length);
    return randomIndex;
  }

  /**
   *
   * @param index
   */
  registerPointAsUsed(index) {
    const point = this.stash[index];

    this.attaks.push({ point, index });
    this.stash.splice(index, 1);
  }

  /**
   *
   * @return {{} | *}
   */
  get lastAttack() {
    if (!this.attaks.length) {
      return {};
    }

    return this.attaks[this.attaks.length - 1];
  }

  /**
   *
   * @param {string} result
   */
  registerAttackResult(result) {
    this.attaks[this.attaks.length - 1].result = result;
  }

  /**
   * Returns string name for team.
   * @return {string} for example: 'Navi'
   */
  static getName() {
    return 'Igor';
  }
}

export default Igor;
