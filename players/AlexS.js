class AlexS {
  constructor() {
    this.sea = [];
    for (let index = 0; index < 10; index++) {
      this.sea.push('0'.repeat(10).split(''));
    }

    this.shotSequence = [];
    this.shipRemaining = {
      1: 4,
      2: 3,
      3: 2,
      4: 1
    };
  }

  /**
   * Returns string name for team.
   * @return {string} for example: 'Navi'
   */
  static getName() {
    return "Alexandru Scripnic";
  }

  /**
   * MISS -> missed attack
   * HIT ->  hit ship but ship is not sunk yet
   * SUNK -> ship is dead
   * DUPLICATED -> once again you attacked this coordinates
   * @param {String} result one of HIT, MISS, SUNK, DUPLICATED.
   * @return {void}
   */
  attackResult(result) {
    switch (result.toUpperCase()) {
      case 'HIT':
        this.shipHit();
        break;
      case 'SUNK':
        this.shipSunk();
        break;
      case 'MISS':
        this.shipMiss();
        break;
    }
  }

  shipMiss() {
    const [l, c] = this.shotSequence.pop();
    this.sea[l][c] = 1;
  }

  shipHit() {
    const [l, c] = this.shotSequence[this.shotSequence.length - 1];

    // console.log(AlexS.fromNumbers([l, c]));

    this.sea[l][c] = 1;
  }

  shipSunk() {
    const [shot1, shot2] = this.shotSequence;
    const lastShot = this.shotSequence[this.shotSequence.length - 1];
    this.shipRemaining[this.shotSequence.length]--;

    const vertical = this.shotSequence.length === 1 || shot1[0] !== shot2[0];
    const [a, b] = shot1;
    const [c, d] = lastShot;

    this.sea[c][d] = 1;

    if (vertical) {
      if (this.sea[a]) {
        this.sea[a][b - 1] = 1;
      }

      if (this.sea[a]) {
        this.sea[a][d + 1] = 1;
      }

      for (let index = b - 1; index < d + 2; index++) {
        if (this.sea[a - 1]) {
          this.sea[a - 1][index] = 1;
        }

        if (this.sea[a + 1]) {
          this.sea[a + 1][index] = 1;
        }
      }
    } else {
      if (this.sea[a - 1]) {
        this.sea[a - 1][b] = 1;
      }

      if (this.sea[c + 1]) {
        this.sea[c + 1][b] = 1;
      }

      for (let index = a - 1; index < c + 2; index++) {
        if (this.sea[index]) {
          this.sea[index][b - 1] = 1;
          this.sea[index][b + 1] = 1;
        }
      }
    }

    this.shotSequence = [];
  }

  /**
   * Returns coordinates in which player want attack
   * First char is Letter=(A...J) rest chars represents a number=(0...10)
   * @return {string} for example: 'B10'
   */
  attack() {
    const nextShot = this.getNextShot();

    this.shotSequence.push(nextShot);

    return AlexS.fromNumbers(nextShot);
  }

  getNextShot() {
    if (this.shotSequence.length) {
      return this.getNextShotBasedOnSequence();
    }

    return this.findNextSpot();
  }

  getNextShotBasedOnSequence() {
    const lastHit = this.shotSequence[this.shotSequence.length - 1];

    if (this.shotSequence.length >= 2) {
      const preLastHit = this.shotSequence[this.shotSequence.length - 2];

      return [
        lastHit[0] !== preLastHit[0] ? lastHit[0] + 1 : lastHit[0],
        lastHit[1] !== preLastHit[1] ? lastHit[1] + 1: lastHit[1]
      ];
    }

    const minAvailable = Object.keys(this.shipRemaining)
      .reduce((min, key) => this.shipRemaining[key] > 0 && min > key ? this.shipRemaining[key] : min, 0);

    let spaceAvailable = 0;
    let [line1, column1] = lastHit;

    column1++;
    while (column1 < 10 && this.sea[line1][column1] !== 1) {
      spaceAvailable++;
      column1++;
    }

    if (spaceAvailable > minAvailable) {
      return [
        lastHit[0],
        lastHit[1] + 1
      ];
    } else {
      return [
        lastHit[0] + 1,
        lastHit[1]
      ];
    }
  }

  findNextSpot() {
    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        if (this.sea[i][j] == 0) {
          return [i, j];
        }
      }
    }

    throw new Error('Algorithm is wrong');
  }

  /**
   * @param position
   * @returns {number[]}
   */
  static fromPosition(position) {
    const el = position.split('');

    return [
      AlexS.fromLetterToNumber(el[0]),
      el[1] - 1
    ];
  }

  /**
   * @param numbers
   * @returns {string}
   */
  static fromNumbers(numbers) {
    return `${ AlexS.fromNumberToLetter(numbers[0]) }${ parseInt(numbers[1]) + 1 }`;
  }

  /**
   * Transform first position to number
   * @param position
   * @returns {number}
   */
  static fromLetterToNumber(position) {
    return position.toUpperCase().charCodeAt(0) - 65;
  }

  /**
   * @param number
   * @returns {string}
   */
  static fromNumberToLetter(number) {
    return String.fromCharCode(number + 65);
  }
}


export default AlexS;
