class Monika {
  constructor() {
    this.counter = 0;
  }

  /**
   * Returns string name for team.
   * @return {string} for example: 'Navi'
   */
  static getName() {
    return "Monika";
  }

  /**
   * MISS -> missed attack
   * HIT ->	hited ship but ship is not sunk yet
   * SUNK -> ship is dead
   * DUPLICATED -> once again you attacked this coordinates
   * @param {String} result one of HIT, MISS, SUNK, DUPLICATED.
   * @return {void}
   */
  attackResult(result) {
    this.counter++;
  }

  /**
   * Returns coordinates in which player want attack
   * First char is Letter=(A...J) rest chars represents a number=(0...10)
   * @return {string} for example: 'B10'
   */
  attack() {
    const i = Math.floor(this.counter / 10);
    const j = this.counter % 10;

    const strI = String.fromCharCode(65 + i);
    const strJ = (j + 1).toString();

    return strI + strJ;
  }
}


export default Monika;
