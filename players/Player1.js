class Player1 {
  /**
   * Returns string name for team.
   * @return {string} for example: 'Navi'
   */
  static getName() {
    return "Ughey";
  }

  /**
   * MISS -> missed attack
   * HIT ->	hited ship but ship is not sunk yet
   * SUNK -> ship is dead
   * DUPLICATED -> once again you attacked this coordinates
   * @param {String} result one of HIT, MISS, SUNK, DUPLICATED.
   * @return {void}
   */
  attackResult(result) {
    // poh
  }

  /**
   * Returns coordinates in which player want attack
   * First char is Letter=(A...J) rest chars represents a number=(0...10)
   * @return {string} for example: 'B10'
   */
  attack() {
    const strI = String.fromCharCode(65 + Math.floor(Math.random() * 10));
    const strJ = Math.ceil(Math.random() * 10).toString();

    return strI + strJ;
  }
}


export default Player1;