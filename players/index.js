import Player1 from './Player1';
import Igor from './Igor';
import Monika from './Monika';
import AlexS from './AlexS';

const players = [
  Player1,
  AlexS,
  Igor,
  Monika,
];

export default players;
